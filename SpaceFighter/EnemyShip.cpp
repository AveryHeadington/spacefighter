
#include "EnemyShip.h"
#include "Level.h"

// EnemyShip that are attacking you
//Setting up the stats for the base enemyship
EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}

//Updates the EnemyShips position so it knows where it is and if it has a collision with something.
	 // (fire weapons, collide with objects, etc.)
void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{
			//Updates if the enemyship is Activated
			GameObject::Activate();
		}
	}
	// has it activated if the enemyship is on the screen and when its not it is deactivated
	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}

// is set if something is hit it will do damage
void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}


void EnemyShip::Deactivate()
{
	if (m_islastenemy)
	{
		GetCurrentLevel()->SetLevelComplete();
	}
	Ship::Deactivate();
}