#pragma once

#include "EnemyShip.h"

class AdvancedEnemyShip : public EnemyShip
{

public:

	AdvancedEnemyShip();
	virtual ~AdvancedEnemyShip() { }

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);


private:

	Texture* m_pTexture;

};