
#pragma once

#include "Ship.h"

class EnemyShip : public Ship
{

public:

	EnemyShip(); // EnemyShip that are attacking you
	virtual ~EnemyShip() { }

	//Updates the EnemyShips position so it knows where it is and if it has a collision with something.
	virtual void Update(const GameTime *pGameTime);

	//Draws the sprite on the game screen
	virtual void Draw(SpriteBatch *pSpriteBatch) = 0;  // Pure virtual function 1)

	virtual void Initialize(const Vector2 position, const double delaySeconds);

	//If the enemy ship fired a blaster it would be here
	virtual void Fire() { }

	virtual void Hit(const float damage);

	virtual std::string ToString() const { return "Enemy Ship"; }

	//Makes sure that if an EnemyShip hits you it knows its an enemy that does damage.
	virtual CollisionType GetCollisionType() const { return CollisionType::ENEMY | CollisionType::SHIP; }

	virtual void SetLastEnemy() { m_islastenemy = true;}

	virtual void Deactivate();

protected:

	//Delay for the ship
	virtual double GetDelaySeconds() const { return m_delaySeconds; }


private:

	double m_delaySeconds;

	double m_activationSeconds;

	bool m_islastenemy = false;
};
