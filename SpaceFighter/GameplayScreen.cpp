
#include "GameplayScreen.h"
#include "Level.h"
#include "Level01.h"
#include "Level02.h"
#include "Level03.h"

GameplayScreen::GameplayScreen(int levelIndex)
{
	m_pLevel = nullptr;
	LoadLevel(levelIndex);

	SetTransitionInTime(1.0f);
	SetTransitionOutTime(0.5f);

	Show();
}

void GameplayScreen::LoadContent(ResourceManager *pResourceManager)
{
	m_pResourceManager = pResourceManager;
	m_pLevel->LoadContent(pResourceManager);
}

void GameplayScreen::HandleInput(const InputState *pInput)
{
	m_pLevel->HandleInput(pInput);
}

void GameplayScreen::LoadLevel(int levelIndex)
{
	switch (levelIndex)
	{
	case 0: m_pLevel = new Level01(); break;
	case 1: m_pLevel = new Level02(); break;
	case 2: m_pLevel = new Level03(); break;
	}
}

void GameplayScreen::Update(const GameTime *pGameTime)
{
	m_pLevel->Update(pGameTime);
	
	if (m_pLevel-> IsLevelComplete()) 
	{
		int index = m_pLevel->GetNextLevelIndex();
		delete m_pLevel;
		LoadLevel(index);
		m_pLevel->LoadContent(m_pResourceManager);
	}
}

void GameplayScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();

	m_pLevel->Draw(pSpriteBatch);

	pSpriteBatch->End();
}
