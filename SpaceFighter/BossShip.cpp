
#include "BossShip.h"

//Setting up the base stats for the big enemy ship
BossShip::BossShip()
{
	SetSpeed(90);
	SetMaxHitPoints(103);
	SetCollisionRadius(90);
}

// (fire weapons, collide with objects, etc.)
void BossShip::Update(const GameTime* pGameTime)
{
	// has it activated if the Bioenemyship is on the screen and when its not it is deactivated
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		float y = GetSpeed() * pGameTime->GetTimeElapsed();
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		if (GetPosition().Y > 200) y = 0;
		TranslatePosition(x, y);
		
		FireWeapons();

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}

//Draws the sprite on the game screen
void BossShip::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}