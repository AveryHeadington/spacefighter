#pragma once

#include "EnemyShip.h"

class KamikazeEnemyShip : public EnemyShip
{

public:

	KamikazeEnemyShip();
	virtual ~KamikazeEnemyShip() { }

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void Draw(SpriteBatch* pSpriteBatch);


private:

	Texture* m_pTexture;

};