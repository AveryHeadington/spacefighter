#include "Level02.h"
#include "KamikazeEnemyShip.h"
#include "BioEnemyShip.h"
#include "AdvancedEnemyShip.h"
#include "Blaster.h"
#include "Level.h"
#include "EnemyBlaster.h"

//Test

void Level02::LoadContent(ResourceManager* pResourceManager)
{
	// Setup enemy ships
	Texture* pTexture = pResourceManager->Load<Texture>("Textures\\KamikazeEnemyShip.png");
	Texture* pTexture2 = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	Texture* pTexture3 = pResourceManager->Load<Texture>("Textures\\AdvancedEnemyShip.png");

	const int COUNT = 40;

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55,
		0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5,
		0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5,
		0.35, 0.45, 0.55
	};

	double delays[COUNT] =
	{
		1.0, 1.25, 1.25, // Adjust the spawn time for the bio enemies
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3,
		2.5, 0.35, 0.35, 0.35, 0.35, 0.35, 0.35, 0.35,
		2.5, 0.35, 0.35, 0.35, 0.35, 0.35, 0.35, 0.35,
		1.5, 0.15, 0.15
	};

	float delay = 3.0; // start delay
	Vector2 position;

	
	EnemyShip* pEnemy;
	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		if (i >= 21 && i < 31) {
			KamikazeEnemyShip* pKEnemy = new KamikazeEnemyShip();
			pEnemy = pKEnemy;
			pKEnemy->SetTexture(pTexture);
			pEnemy->SetCurrentLevel(this);
			pEnemy->Initialize(position, (float)delay);
			AddGameObject(pEnemy);
		}
		else if (i >= 31) {
			AdvancedEnemyShip* pAEnemy = new AdvancedEnemyShip();
			pEnemy = pAEnemy;
			EnemyBlaster* pEnemyBlaster = new EnemyBlaster(true);
			pEnemyBlaster->SetProjectilePool(&m_projectiles);
			pEnemy->AttachWeapon(pEnemyBlaster, Vector2::UNIT_Y * 10);
			pAEnemy->SetTexture(pTexture3);
			pEnemy->SetCurrentLevel(this);
			pEnemy->Initialize(position, (float)delay);
			AddGameObject(pEnemy);
		}
		else {
			BioEnemyShip* pBEnemy = new BioEnemyShip();
			pEnemy = pBEnemy;
			pBEnemy->SetTexture(pTexture2);
			pEnemy->SetCurrentLevel(this);
			pEnemy->Initialize(position, (float)delay);
			AddGameObject(pEnemy);
			
		}
		
	}
	pEnemy->SetLastEnemy();

	Level::LoadContent(pResourceManager);
}