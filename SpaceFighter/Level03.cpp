#include "Level03.h"
#include "BossShip.h"
#include "Blaster.h"
#include "EnemyBlaster.h"


void Level03::LoadContent(ResourceManager* pResourceManager)
{
	// Setup enemy ships
	Texture* pTexture = pResourceManager->Load<Texture>("Textures\\EnemyBossShip.png");
	//Texture* pTexture2 = pResourceManager->Load<Texture>("Textures\\EnemyBossShipLW.png");
	//Texture* pTexture3 = pResourceManager->Load<Texture>("Textures\\EnemyBossShipNW.png");

	const int COUNT = 1;

	double xPositions[COUNT] =
	{
		0.50
	};

	double delays[COUNT] =
	{
		1.0
	};

	float delay = 3.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BossShip* pEnemy = new BossShip();
		EnemyBlaster* pEnemyBlaster = new EnemyBlaster(true);
		pEnemyBlaster->SetProjectilePool(&m_projectiles);
		EnemyBlaster* pEnemyBlaster2 = new EnemyBlaster(true);
		pEnemyBlaster2->SetProjectilePool(&m_projectiles);
		EnemyBlaster* pEnemyBlaster3 = new EnemyBlaster(true);
		pEnemyBlaster3->SetProjectilePool(&m_projectiles);
		pEnemy->SetTexture(pTexture);
		pEnemy->AttachWeapon(pEnemyBlaster, Vector2::UNIT_Y * 10);
		pEnemy->AttachWeapon(pEnemyBlaster2, Vector2(65, 10));
		pEnemy->AttachWeapon(pEnemyBlaster3, Vector2(-65, 10));
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}