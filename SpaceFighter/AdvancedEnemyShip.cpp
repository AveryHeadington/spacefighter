#include "AdvancedEnemyShip.h"

//Setting up the base stats for the big enemy ship
AdvancedEnemyShip::AdvancedEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(5);
	SetCollisionRadius(20);
}

// (fire weapons, collide with objects, etc.)
void AdvancedEnemyShip::Update(const GameTime* pGameTime)
{
	// has it activated if the Bioenemyship is on the screen and when its not it is deactivated
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		FireWeapons();

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}

//Draws the sprite on the game screen
void AdvancedEnemyShip::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}