
#pragma once

#include "Weapon.h"
#include "Projectile.h"

class EnemyBlaster : public Weapon
{

public:

	EnemyBlaster(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0;
		m_cooldownSeconds = 1.0; // change the player weapon's cool down time to allow the ship to shoot faster
	}

	virtual ~EnemyBlaster() { }

	virtual void Update(const GameTime* pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(TriggerType triggerType)
	{
		if (IsActive() && CanFire())
		{
			if (triggerType.Contains(GetTriggerType()))
			{
				Projectile* pProjectile = GetProjectile();
				
				if (pProjectile)
				{
					pProjectile->SetDirection(Vector2::UNIT_Y);
					pProjectile->Activate(GetPosition(), false);
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}


private:

	float m_cooldown;
	float m_cooldownSeconds;

};